"use strict";

$(document).ready(function () {


  //Полифилл для отображения svg иконок в IE
  svg4everybody();

  //активация фиксированного нав.меню
  $(window).on("scroll", function () {
    if ($(window).scrollTop() > 200) {
      $(".navbar").addClass("navbar--stick");
    } else {
      $(".navbar").removeClass("navbar--stick");
    }
  });

  // Закрытие и открытие нав. меню
  $(".nav-list__button--close").on("click", function () {
    $(".navbar__nav-list").addClass("nav-list--hidden");
  });


  $(".nav-list__button--open").on("click", function (e) {
    e.preventDefault();
    e.stopPropagation();

    $(".navbar__nav-list").removeClass("nav-list--hidden");

    $(document).one('click', function closeMenu(e) {
      if ($(".navbar").has(e.target).length === 0) {
        $(".navbar__nav-list").addClass("nav-list--hidden");
      } else {
        $(document).one("click", closeMenu);
      }
    });
  });

  //Выделение активного пункта меню
  $(".nav-list__item").on("click", function () {
    $(".navbar__nav-list .nav-list__item").removeClass("nav-list__item--active");
    $(this).addClass("nav-list__item--active");
  });

  // Скроллинг к секции при нажатии на пункт меню
  var $root = $("html, body");
  $(".nav-list__item a, .header__button").on("click", function (event) {
    event.preventDefault();
    var $anchor = $(this);
    $root.stop().animate({
      scrollTop: ($($anchor.attr("href")).offset().top - 150)
    }, 800);
  });

  //Выделение пункта меню при скроллинге
  var menuSelectors = ["#header", "#about", "#ingredients", "#menu", "#reviews", "#reservations"];
  menuSelectors.forEach(function (selector) {
    inView.offset(500);
    inView(selector)
      .on("enter", function () {
        $(".navbar__nav-list .nav-list__item").removeClass("nav-list__item--active");
        $('a[href^="' + selector + '"]').parent('.nav-list__item').addClass("nav-list__item--active");
      });
  })




});
