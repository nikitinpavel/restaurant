/*global require*/
"use strict";

var gulp = require("gulp"),
  path = require("path"),
  data = require("gulp-data"),
  pug = require("gulp-pug"),
  sass = require("gulp-sass"),
  uglify = require("gulp-uglify"),
  svgSprite = require('gulp-svg-sprite'),
  prefix = require("gulp-autoprefixer"),
  imagemin = require('gulp-imagemin'),
  browserSync = require("browser-sync").create();

/*
 * Directories here
 */
var paths = {
  public: "./public/",
  sass: "./src/sass/",
  css: "./public/css/",
  js: "./public/js/",
  img: "./public/img/",
  svg: "./public/svg",
  data: "./src/_data/"
};

/**
 * Compile .pug files and pass in data from json file
 * matching file name. index.pug - index.pug.json
 */
gulp.task("pug", function() {
  return gulp
    .src("./src/*.pug")
    .pipe(
      data(function(file) {
        return require(paths.data + path.basename(file.path) + ".json");
      })
    )
    .pipe(
      pug({
        pretty: true
      })
    )
    .on("error", function(err) {
      process.stderr.write(err.message + "\n");
      this.emit("end");
    })
    .pipe(gulp.dest(paths.public));
});

/**
 * Compile .scss files into public css directory With autoprefixer no
 * need for vendor prefixes then live reload the browser.
 */
gulp.task("sass", function() {
  return gulp
    .src(paths.sass + "*.scss")
    .pipe(
      sass({
        includePaths: [paths.sass],
        outputStyle: "compressed"
      })
    )
    .on("error", sass.logError)
    .pipe(
      prefix(["last 2 versions", "> 2%", "ie 11"], {
        cascade: true
      })
    )
    .pipe(gulp.dest(paths.css))
    .pipe(browserSync.stream());
});

// Gulp task to minify JavaScript files
gulp.task("scripts", function() {
  return (
    gulp
      .src("./src/js/**/*.js")
      // Minify the file
      .pipe(uglify())
      // Output
      .pipe(gulp.dest(paths.js))
  );
});
//Gulp task for svg sprites
gulp.task('svgSprite', function () {
  return gulp.src('./src/svg/*.svg') 
    .pipe(svgSprite({
      mode: {
        stack: {
          sprite: "../sprite.svg"  
        }
      },
    }
    ))
    .pipe(gulp.dest(paths.svg));
});

//Gulp task for minify Image files

function imgMin() {
  return gulp.src("./src/img/*")
    .pipe(imagemin([
      imagemin.gifsicle({ interlaced: true }),
      imagemin.jpegtran({ progressive: true }),
      imagemin.optipng({ optimizationLevel: 4 }),
      imagemin.svgo({
        plugins: [
          { removeViewBox: false },
          { cleanupIDs: false }
        ]
      })
    ]))
    .pipe(gulp.dest(paths.img));
}

gulp.task("imgMin", imgMin);


/**
 * Watch changes in files
 */

gulp.task("watch", function() {
  gulp.watch(paths.sass + "**/*.scss", gulp.series("sass"));
  gulp.watch("./src/**/*.pug", gulp.series("pug"));
  gulp.watch("./src/**/*.js", gulp.series("scripts"));
  gulp.watch("./src/img/*", gulp.series("imgMin"));
  gulp.watch("./src/svg/*", gulp.series("svgSprite"));
});

/* Reload browser */
gulp.task("reload", function(done) {
  browserSync.reload();
  done();
});

// Build task compile sass and pug.
gulp.task("build", gulp.series("sass", "pug", "scripts","imgMin"));

// type gulp in command line
//launch server port:3000
//watches changes in src folder and reload browser
gulp.task("default", function() {
  browserSync.init({
    server: {
      baseDir: paths.public
    }
  });

  gulp.watch("./src/**/*pug.json", gulp.series("pug"));
  gulp.watch("./src/**/*.scss", gulp.series("sass"));
  gulp.watch("./src/**/*.pug", gulp.series("pug"));
  gulp.watch("./src/**/*.js", gulp.series("scripts"));
  gulp.watch("./src/img/*", gulp.series("imgMin"));
  gulp.watch("./src/svg/*", gulp.series("svgSprite"));
  gulp.watch("./src/**/*.*", gulp.series("reload"));
});
